var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    rename    = require('gulp-rename'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minifyHTML = require('gulp-minify-html')
    ;



gulp.task('scripts', function () {

        gulp.src('build/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public_html/assets/js'))
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('public_html/assets/js'))
        ;

        var vendorJs = [

        ];

        gulp.src(vendorJs)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('public_html/assets/js'))
        .pipe(uglify())
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest('public_html/assets/js'));
});

gulp.task('style', function() {

    var scssPaths = [];

    var sassOptions = {
        outputStyle: 'expanded',
        includePaths: scssPaths
    };

    gulp.src('build/scss/app.scss')
        .pipe(sass(sassOptions))
        .pipe(autoprefixer({
            browsers: [
                'last 2 versions',
                'ie >= 9',
                'and_chr >= 2.3'
            ],
            cascade: false
        }))
        .pipe(gulp.dest('public_html/assets/css'))
        .pipe(cleanCSS({
            keepSpecialComments: 0,
            shorthandCompacting: false,
            roundingPrecision: -1,
            compatibility: 'ie8'
        }))
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('public_html/assets/css'))
    ;
});

gulp.task('minify-html', function() {
    var opts = {};
    return gulp.src('build/html/**.html')
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest('public_html/'));
});


gulp.task('images', function () {
    return gulp.src('build/images/**/*')
        .pipe(gulp.dest('public_html/assets/images'));
});

gulp.task('default', ['style', 'scripts', 'minify-html', 'images']);

gulp.task('watch', function () {
    gulp.watch('build/scss/*.scss', ['style']);
    gulp.watch('build/js/*.js', ['scripts']);
    gulp.watch('build/html/*.html', ['minify-html']);
});